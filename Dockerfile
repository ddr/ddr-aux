ARG ruby_version

FROM ruby:${ruby_version}

ARG node_version="16"

SHELL ["/bin/bash", "-c"]

ENV APP_ROOT="/opt/app-root" \
	APP_USER="app-user" \
	APP_UID="1001" \
	APP_GID="0" \
	BUNDLE_IGNORE_CONFIG="true" \
	BUNDLE_JOBS="4" \
	BUNDLE_USER_HOME="${GEM_HOME}" \
	LANG="en_US.UTF-8" \
	LANGUAGE="en_US:en" \
	RAILS_ENV="production" \
	RAILS_PORT="3000" \
	TZ="US/Eastern"

WORKDIR $APP_ROOT

RUN set -eux ; \
	curl -sL https://deb.nodesource.com/setup_${node_version}.x | bash - ; \
	apt-get -y update ; \
	apt-get -y install less locales nodejs libpq-dev ; \
	apt-get -y clean ; \
	rm -rf /var/lib/apt/lists/* ; \
	\
	npm install -g yarn ; \
	\
	echo "$LANG UTF-8" >> /etc/locale.gen ; \
	locale-gen $LANG ; \
	\
	useradd -r -u $APP_UID -g $APP_GID -d $APP_ROOT -s /sbin/nologin $APP_USER

COPY . .

RUN gem install bundler -v "$(tail -1 Gemfile.lock | tr -d ' ')" && \
	bundle install

RUN SECRET_KEY_BASE="1" ./bin/rails assets:precompile

RUN chmod -R g=u .

EXPOSE $RAILS_PORT

USER $APP_USER

CMD ["./bin/rails", "server"]

require File.expand_path('../config/application', __FILE__)

Rails.application.load_tasks

desc "Create admin user"
task :create_admin => :environment do
  username = ENV.fetch("DDR_AUX_ADMIN_USER", "admin")
  if User.exists?(username: username)
    puts "User '#{username}' already exists."
  else
    password = ENV.fetch("DDR_AUX_ADMIN_PASSWORD", Devise.friendly_token)
    User.create!(username: username, admin: true, password: password)
    puts "Created user '#{username}' with password '#{password}'."
  end
end

namespace :api do
  desc "Generate an API key for a user"
  task :generate_key, [:username] => :environment do |t, args|
    user = User.find_by_username!(args[:username])
    if user.api_key
      puts "User #{user} already has API key: #{user.api_key}"
    else
      user.generate_api_key!
      puts "API key for #{user}: #{user.api_key}"
    end
  end
end

namespace :test do
  desc "Run API integration tests"
  task :integration => :environment do
    system "rails server -e test -d"
    system "rspec ./test/integration/"
    pid = `cat tmp/pids/server.pid`
    system "kill -QUIT #{pid}"
  end
end

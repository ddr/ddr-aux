# ddr-aux API v1

- [Admin Sets](#admin-sets)
- [Alerts](#alerts)
- [Job Queues](#job-queues)
- [Languages](#languages)
- [Rights Statements](#rights-statements)

## Admin Sets

### GET /api/admin_sets

```json
[
  {
    "id": 1,
    "code": "researchdata",
    "title": "Duke Research Data",
    "created_at": "2019-08-01T06:56:06.542-04:00",
    "updated_at": "2019-08-01T06:56:06.542-04:00"
  },
  {
    "id": 2,
    "code": "duke_scholarship",
    "title": "Duke Scholarship",
    "created_at": "2019-08-01T06:56:06.545-04:00",
    "updated_at": "2019-08-01T06:56:06.545-04:00"
  }
]
```

Responses:

  - 200 JSON array

### GET /api/admin_sets/find

Responses:

  - 200 JSON object
  - 404

### GET /api/admin_sets/{ID}

```json
{
  "id": 1,
  "code": "researchdata",
  "title": "Duke Research Data",
  "created_at": "2019-08-01T06:56:06.542-04:00",
  "updated_at": "2019-08-01T06:56:06.542-04:00"
}
```

Responses:

  - 200 JSON object
  - 404

## Alerts

### GET /api/alerts

Responses:

- 200 JSON array

### GET /api/alerts/active

Responses:

- 200 JSON array

### GET /api/alerts/{ID}

Responses:

- 200 JSON object
- 404

## Job Queues

### GET /api/job_queues

```json
[
  {
    "id": 1,
    "name": "default",
    "description": "Default queue",
    "workers": 1,
    "enabled": true,
    "created_at": "2019-08-15T17:45:47.766-04:00",
    "updated_at": "2019-08-15T17:45:47.766-04:00"
  },
  {
    "id": 2,
    "name": "publication",
    "description": "Publication queue",
    "workers": 1,
    "enabled": true,
    "created_at": "2019-08-15T17:47:33.361-04:00",
    "updated_at": "2019-08-15T17:47:33.361-04:00"
  },
  ...
]
```

Responses:

- 200 JSON array

### GET /api/job_queues/active

```json
[
  {
    "id": 1,
    "name": "default",
    "description": "Default queue",
    "workers": 1,
    "enabled": true,
    "created_at": "2019-08-15T17:45:47.766-04:00",
    "updated_at": "2019-08-15T17:45:47.766-04:00"
  },
  {
    "id": 2,
    "name": "publication",
    "description": "Publication queue",
    "workers": 1,
    "enabled": true,
    "created_at": "2019-08-15T17:47:33.361-04:00",
    "updated_at": "2019-08-15T17:47:33.361-04:00"
  },
  ...
]
```

Responses:

- 200 JSON array

### GET /api/job_queues/resque_pool_config

```json
{
  "default": 1,
  "publication": 1,
  "export": 1,
  "derivatives": 1,
  "file_characterization": 1,
  "aspace": 1,
  "fixity": 1,
  "nested_folder_ingest": 1,
  "standard_ingest": 1,
  "batch": 1,
  "file_upload": 1,
  "structure": 2
}
```

Responses:

- 200 JSON object

### GET /api/job_queues/{ID}

```json
{
  "id": 1,
  "name": "default",
  "description": "Default queue",
  "workers": 1,
  "enabled": true,
  "created_at": "2019-08-15T17:45:47.766-04:00",
  "updated_at": "2019-08-15T17:45:47.766-04:00"
}
```

Responses:

- 200 JSON object
- 404

## Languages

### GET /api/languages

```json
[
  {
	"id": 1,
	"code": "asm",
	"label": "Assamese",
	"created_at": "2019-08-01T06:56:06.582-04:00",
	"updated_at": "2019-08-01T06:56:06.582-04:00"
  },
  ...
]
```

Responses:

- 200 JSON array

### GET /api/languages/find

Responses:

- 200 JSON object
- 404

### GET /api/languages/{ID}

```json
{
  "id": 1,
  "code": "asm",
  "label": "Assamese",
  "created_at": "2019-08-01T06:56:06.582-04:00",
  "updated_at": "2019-08-01T06:56:06.582-04:00"
}
```

Responses:

- 200 JSON object
- 404

## Rights Statements

### GET /api/rights_statements

```json
[
  {
    "id": 1,
    "title": "Creative Commons Attribution 4.0 International",
    "url": "https://creativecommons.org/licenses/by/4.0/",
    "short_title": "CC BY 4.0",
    "feature": [
      "cc-cc",
      "cc-by"
    ],
    "reuse_text": "Free Re-Use",
    "created_at": "2019-08-01T06:56:06.448-04:00",
    "updated_at": "2019-08-01T06:56:06.448-04:00"
  },
  {
    "id": 2,
    "title": "Creative Commons Attribution-NonCommercial 4.0 International",
    "url": "https://creativecommons.org/licenses/by-nc/4.0/",
    "short_title": "CC BY-NC 4.0",
    "feature": [
      "cc-cc",
      "cc-by",
      "cc-nc"
    ],
    "reuse_text": "Limited Re-Use",
    "created_at": "2019-08-01T06:56:06.453-04:00",
    "updated_at": "2019-08-01T06:56:06.453-04:00"
  },
  ...
]
```

Responses:

- 200 JSON array

### GET /api/rights_statements/find

Responses:

- 200 JSON object
- 404

### GET /api/rights_statements/{ID}

```json
{
  "id": 1,
  "title": "Creative Commons Attribution 4.0 International",
  "url": "https://creativecommons.org/licenses/by/4.0/",
  "short_title": "CC BY 4.0",
  "feature": [
    "cc-cc",
    "cc-by"
  ],
  "reuse_text": "Free Re-Use",
  "created_at": "2019-08-01T06:56:06.448-04:00",
  "updated_at": "2019-08-01T06:56:06.448-04:00"
}
```

Responses:

- 200 JSON object
- 404

module Api::V1
  class JobQueuesController < ModelController

    def active
      render json: JobQueue.active
    end

    def resque_pool_config
      render json: JobQueue.resque_pool_config
    end

    def index
      if request.method == 'GET'
        super
      else
        index_patch
      end
    end

    protected

    def index_patch
      case params.require(:op)
      when "pause" then pause
      when "unpause" then unpause
      else forbidden
      end
    end

    def pause
      JobQueue.pause!
      render json: JobQueue.paused
    end

    def unpause
      JobQueue.unpause!
      render json: JobQueue.unpaused
    end

  end
end

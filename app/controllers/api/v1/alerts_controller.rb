module Api::V1
  class AlertsController < ModelController

    def active
      alerts = ::Alert.active.where(params.permit(:site))
      render json: alerts
    end

  end
end

class HealthCheckController < ApplicationController

  # HEAD /health
  def index
    _ = ActiveRecord::Base.connection
    head :ok
  rescue PG::ConnectionBad
    head :internal_server_error
  end

end

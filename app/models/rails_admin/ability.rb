module RailsAdmin
  class Ability
    include CanCan::Ability

    attr_reader :user

    def initialize(user)
      @user = user || User.new

      # common abilities
      can :access, :rails_admin
      can :read, :dashboard

      # aliases
      alias_action :export, to: :read

      if user.admin?
        can :manage, :all
      else
        can :read, :all
        cannot :read, [ User, ManagerPermission ]
        can :manage, manageable_models
      end
    end

    def manageable_models
      user.manager_permissions.map { |mp| mp.model.constantize }
    end

  end
end

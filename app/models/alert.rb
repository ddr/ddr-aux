class Alert < ApplicationRecord

  SITES = %w( public admin )

  validates_inclusion_of :site, in: SITES
  validates_presence_of :start_at, :stop_at, :message
  validate :valid_alert_period

  scope :active, ->{ where(enabled: true).where("? BETWEEN start_at AND stop_at", DateTime.now) }

  protected

  def valid_alert_period
    return unless start_at && stop_at
    unless start_at < stop_at
      errors.add(:start_at, "must be earlier than stop_at")
    end
  end

end

class JobQueue < ApplicationRecord

  MAX_WORKERS = 100

  validates_uniqueness_of :name
  validates_inclusion_of :workers, in: (1..MAX_WORKERS)

  scope :enabled, ->{ where(enabled: true) }
  scope :disabled, ->{ where(enabled: false) }
  scope :paused, ->{ where(paused: true) }
  scope :unpaused, ->{ where(paused: false) }
  scope :active, ->{ enabled.unpaused }

  def self.resque_pool_config
    active.pluck(:name, :workers).to_h
  end

  def self.pause!
    enabled.each(&:pause!)
  end

  def self.unpause!
    paused.each(&:unpause!)
  end

  # def enable!
  #   update!(enabled: true)
  # end

  # def disable!
  #   update!(enabled: false)
  # end

  def pause!
    update!(paused: true)
  end

  def unpause!
    update!(paused: false)
  end

end

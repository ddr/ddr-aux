class ManagerPermission < ApplicationRecord

  MANAGEABLE_MODELS = %w( AdminSet Alert Contact JobQueue Language RightsStatement ).freeze

  belongs_to :user
  enum model: MANAGEABLE_MODELS

end

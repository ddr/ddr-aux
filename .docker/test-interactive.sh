#!/bin/bash
#
# Run an interactive test environment with the
# local code mounted in the app container.
#
context=$(git rev-parse --show-toplevel)
cd "$(dirname ${BASH_SOURCE[0]})"
./test.sh stop app

./test.sh run --rm -v "${context}:/opt/app-root" -u app-user app bash

./test.sh down

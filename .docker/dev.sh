#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

export RAILS_ENV=development

docker-compose \
    -p ddr-aux-dev \
    -f docker-compose.yml \
    -f docker-compose.dev.yml \
    "$@"

#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"
source .env
export APP_IMAGE
export RAILS_ENV=development
stack="ddr-aux-${RAILS_ENV}"
options=( "$@" )

if [[ "${options[@]}" =~ (up|deploy) ]]; then
    options+=(
	-c docker-compose.yml
	-c docker-compose.dev.yml
    )
fi

docker stack ${options[@]} $stack

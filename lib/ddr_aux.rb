require 'yaml'

module DdrAux

  def self.version
    @version ||= YAML.load_file(File.expand_path('../../version.yml', __FILE__))['variables']['VERSION']
  end

end

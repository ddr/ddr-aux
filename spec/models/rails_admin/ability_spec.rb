require 'rails_helper'
require 'cancan/matchers'

RSpec.describe RailsAdmin::Ability do

  subject { described_class.new(user) }

  describe "admin user abilities" do
    let(:user) { FactoryBot.create(:user, :admin) }

    it { is_expected.to be_able_to(:read, :dashboard) }
    it { is_expected.to be_able_to(:access, :rails_admin) }

    it { is_expected.to be_able_to(:manage, AdminSet) }
    it { is_expected.to be_able_to(:manage, Alert) }
    it { is_expected.to be_able_to(:manage, Contact) }
    it { is_expected.to be_able_to(:manage, JobQueue) }
    it { is_expected.to be_able_to(:manage, Language) }
    it { is_expected.to be_able_to(:manage, RightsStatement) }

    it { is_expected.to be_able_to(:manage, User) }
    it { is_expected.to be_able_to(:manage, ManagerPermission) }
  end

  describe "non-admin user abilities" do
    let(:user) { FactoryBot.create(:user) }

    it { is_expected.to be_able_to(:read, :dashboard) }
    it { is_expected.to be_able_to(:access, :rails_admin) }

    [ AdminSet, Alert, Contact, JobQueue, Language, RightsStatement ].each do |model|
      it { is_expected.to be_able_to(:read, model) }
      it { is_expected.not_to be_able_to(:create, model) }
      it { is_expected.not_to be_able_to(:update, model) }
      it { is_expected.not_to be_able_to(:destroy, model) }
    end

    it { is_expected.not_to be_able_to(:read, User) }
    it { is_expected.not_to be_able_to(:read, ManagerPermission) }

    ManagerPermission::MANAGEABLE_MODELS.each do |model|
      describe "with manager permission on #{model}" do
        before { ManagerPermission.create(user: user, model: model) }
        it { is_expected.to be_able_to(:manage, model.constantize) }
      end
    end
  end

end

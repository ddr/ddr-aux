require 'rails_helper'

RSpec.describe JobQueue do

  describe ".resque_pool_config" do
    subject { described_class.resque_pool_config }
    it { is_expected.to eq({"default"=>1}) }
  end

  describe ".pause!" do
    it "pauses an enabled (unpaused) queue" do
      queue = described_class.create(name: "foo")
      expect { described_class.pause!; queue.reload }
        .to change(queue, :paused).from(false).to(true)
    end
    it "does not pause a disabled queue" do
      queue = described_class.create(name: "foo", enabled: false)
      expect { described_class.pause!; queue.reload }
        .not_to change(queue, :paused)
    end
    it "does not pause a paused queue" do
      queue = described_class.create(name: "foo", paused: true)
      expect { described_class.pause!; queue.reload }
        .not_to change(queue, :paused)
    end
  end

  describe ".unpause!" do
    it "does not unpause an enabled (unpaused) queue" do
      queue = described_class.create(name: "foo")
      expect { described_class.unpause!; queue.reload }
        .not_to change(queue, :paused)
    end
    it "does not unpause a disabled queue" do
      queue = described_class.create(name: "foo", enabled: false)
      expect { described_class.unpause!; queue.reload }
        .not_to change(queue, :paused)
    end
    it "unpauses a paused queue" do
      queue = described_class.create(name: "foo", paused: true)
      expect { described_class.unpause!; queue.reload }
        .to change(queue, :paused).from(true).to(false)
    end
  end

end

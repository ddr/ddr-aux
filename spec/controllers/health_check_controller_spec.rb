require 'rails_helper'

RSpec.describe HealthCheckController, type: :controller do

  describe "when there is a db connection problem" do
    it "returns a 500 status code" do
      allow(ActiveRecord::Base).to receive(:connection).and_raise(PG::ConnectionBad)
      head :index
      expect(response.response_code).to eq(500)
    end
  end

  describe "when the db connection is functional" do
    it "returns a 200 status code" do
      head :index
      expect(response.response_code).to eq(200)
    end
  end

end

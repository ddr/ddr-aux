require "rails_helper"
require "support/api/v1/shared_examples_for_model_controllers"

module Api::V1
  RSpec.describe JobQueuesController do

    it_behaves_like "an API v1 model controller"

    describe "#resque_pool_config"

    describe "#active"

    describe "#patch" do
      describe "op=pause" do
        it "pauses the enabled queues" do
          expect(JobQueue).to receive(:pause!)
          patch :index, params: { op: "pause" }
        end
      end
      describe "op=unpause" do
        it "unpauses the queues" do
          expect(JobQueue).to receive(:unpause!)
          patch :index, params: { op: "unpause" }
        end
      end
      describe "invalid op" do
        it "is forbidden" do
          patch :index, params: { op: "foo" }
          expect(response).to be_forbidden
        end
      end
    end

  end
end

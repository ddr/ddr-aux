require "rails_helper"

module Api::V1
  RSpec.describe AlertsController do

    specify "#index action" do
      get :index
      expect(response.response_code).to eq(200)
      expect(response.content_type).to eq("application/json")
    end

    specify "#show action" do
      alert = Alert.create(site: "public", start_at: 1.day.ago, stop_at: 1.day.from_now, message: "Alert!")
      get :show, params: {id: alert.id}
      expect(response.response_code).to eq(200)
      expect(response.content_type).to eq("application/json")
      result = JSON.parse(response.body)
      expect(result).to be_a(Hash)
      expect(result["id"]).to eq(alert.id)
    end

    describe "#active" do
      it "returns active alerts" do
        Alert.create(site: "public", start_at: 1.day.ago, stop_at: 1.day.from_now, message: "Alert!")

        get :active, params: {site: "public"}
        result = JSON.parse(response.body)
        expect(result.first["message"]).to eq("Alert!")

        get :active, params: {site: "admin"}
        result = JSON.parse(response.body)
        expect(result).to be_empty
      end

      it "filters out past alerts" do
        Alert.create(site: "public", start_at: 2.days.ago, stop_at: 1.day.ago, message: "Alert!")
        get :active, params: {site: "public"}
        result = JSON.parse(response.body)
        expect(result).to be_empty
      end

      it "filters out future alerts" do
        Alert.create(site: "public", start_at: 1.day.from_now, stop_at: 2.days.from_now, message: "Alert!")
        get :active, params: {site: "public"}
        result = JSON.parse(response.body)
        expect(result).to be_empty
      end

      it "filters out disabled alerts" do
        Alert.create(site: "public", start_at: 1.day.ago, stop_at: 1.day.from_now, enabled: false, message: "Alert!")
        get :active, params: {site: "public"}
        result = JSON.parse(response.body)
        expect(result).to be_empty
      end
    end
  end
end

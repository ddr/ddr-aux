require "rails_helper"
require "support/api/v1/shared_examples_for_model_controllers"

module Api::V1
  RSpec.describe RightsStatementsController do
    it_behaves_like "an API v1 model controller"
  end
end

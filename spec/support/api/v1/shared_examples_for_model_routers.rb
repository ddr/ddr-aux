RSpec.shared_examples "an API v1 model router" do |model|
  it "routes GET /api/#{model}" do
    expect(get: "/api/#{model}")
      .to route_to(controller: "api/v1/#{model}", action: "index", format: "json")
  end

  it "routes GET /api/#{model}/:id" do
    expect(get: "/api/#{model}/1")
      .to route_to(controller: "api/v1/#{model}", action: "show", id: "1", format: "json")
  end
end

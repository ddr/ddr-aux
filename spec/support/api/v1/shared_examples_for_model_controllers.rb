RSpec.shared_examples "an API v1 model controller" do
  specify "#index action" do
    get :index
    expect(response.response_code).to eq(200)
    expect(response.content_type).to eq("application/json")
  end

  specify "#show action" do
    get :show, params: {id: 1}
    expect(response.response_code).to eq(200)
    expect(response.content_type).to eq("application/json")
    result = JSON.parse(response.body)
    expect(result).to be_a(Hash)
    expect(result["id"]).to eq(1)
  end
end

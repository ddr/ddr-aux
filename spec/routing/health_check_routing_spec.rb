require 'rails_helper'

RSpec.describe "health check routing", type: :routing do
  it "routes HEAD /health to health_check#index" do
    expect(head: "/health").to route_to(controller: "health_check", action: "index")
  end
end

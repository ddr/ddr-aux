require "rails_helper"
require "support/api/v1/shared_examples_for_model_routers"

RSpec.describe "API v1 job_queues routing", type: :routing do
  it_behaves_like "an API v1 model router", :job_queues

  specify do
    expect(get: "/api/job_queues/active")
      .to route_to(controller: "api/v1/job_queues", action: "active", format: "json")
  end

  specify do
    expect(get: "/api/job_queues/resque_pool_config")
      .to route_to(controller: "api/v1/job_queues", action: "resque_pool_config", format: "json")
  end

  specify do
    expect(patch: "/api/job_queues")
      .to route_to(controller: "api/v1/job_queues", action: "index", format: "json")
  end

end

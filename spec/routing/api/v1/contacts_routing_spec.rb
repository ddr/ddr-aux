require "rails_helper"
require "support/api/v1/shared_examples_for_model_routers"

RSpec.describe "API v1 contacts routing", type: :routing do
  it_behaves_like "an API v1 model router", :contacts

  it "routes GET /api/contacts/find" do
    expect(get: "/api/contacts/find?code=dc")
      .to route_to(controller: "api/v1/contacts", action: "find", code: "dc", format: "json")
  end
end

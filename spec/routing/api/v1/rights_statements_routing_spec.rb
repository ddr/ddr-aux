require "rails_helper"
require "support/api/v1/shared_examples_for_model_routers"

RSpec.describe "API v1 rights_statements routing", type: :routing do
  it_behaves_like "an API v1 model router", :rights_statements
end

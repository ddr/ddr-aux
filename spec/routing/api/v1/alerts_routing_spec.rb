require "rails_helper"
require "support/api/v1/shared_examples_for_model_routers"

RSpec.describe "API v1 alerts routing", type: :routing do
  it_behaves_like "an API v1 model router", :alerts

  it "routes /api/alerts/active" do
    expect(get: "/api/alerts/active")
      .to route_to(controller: "api/v1/alerts", action: "active", format: "json")
  end
end

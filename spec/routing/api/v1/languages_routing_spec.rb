require "rails_helper"
require "support/api/v1/shared_examples_for_model_routers"

RSpec.describe "API v1 languages routing", type: :routing do
  it_behaves_like "an API v1 model router", :languages

  it "routes GET /api/languages/find" do
    expect(get: "/api/languages/find?code=cym")
      .to route_to(controller: "api/v1/languages", action: "find", code: "cym", format: "json")
  end
end

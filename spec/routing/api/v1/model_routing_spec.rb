require "rails_helper"

RSpec.describe "API v1 model routing", type: :routing do
  models = %w( admin_sets alerts contacts languages rights_statements )

  describe "index routing" do
    models.each do |model|
      it "routes GET /api/#{model}" do
        expect(get: "/api/#{model}")
          .to route_to(controller: "api/v1/#{model}", action: "index", format: "json")
      end
    end
    it "does not route a non-existent model" do
      expect(get: "/api/foobar").not_to be_routable
    end
  end

  describe "show routing" do
    models.each do |model|
      it "routes GET /api/#{model}/:id" do
        expect(get: "/api/#{model}/1")
          .to route_to(controller: "api/v1/#{model}", action: "show", id: "1", format: "json")
      end
      it "does not route a non-existent model" do
        expect(get: "/api/foobar/1").not_to be_routable
      end
    end
  end

  describe "find routing" do
    it "routes GET /api/admin_sets/find" do
      expect(get: "/api/admin_sets/find?code=dc")
        .to route_to(controller: "api/v1/admin_sets", action: "find", code: "dc", format: "json")
    end
    it "routes GET /api/contacts/find" do
      expect(get: "/api/contacts/find?code=dc")
        .to route_to(controller: "api/v1/contacts", action: "find", code: "dc", format: "json")
    end
    it "routes GET /api/languages/find" do
      expect(get: "/api/languages/find?code=cym")
        .to route_to(controller: "api/v1/languages", action: "find", code: "cym", format: "json")
    end
    it "does not route a non-existent model" do
      expect(get: "/api/foobar/find").not_to be_routable
    end
  end

end

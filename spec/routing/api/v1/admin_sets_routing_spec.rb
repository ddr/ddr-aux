require "rails_helper"
require "support/api/v1/shared_examples_for_model_routers"

RSpec.describe "API v1 admin_sets routing", type: :routing do
  it_behaves_like "an API v1 model router", :admin_sets

  it "routes GET /api/admin_sets/find" do
    expect(get: "/api/admin_sets/find?code=dc")
      .to route_to(controller: "api/v1/admin_sets", action: "find", code: "dc", format: "json")
  end
end

require "ddr_aux/api/constraints"

Rails.application.routes.draw do

  root to: 'rails_admin/main#dashboard'

  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  # Logout via GET
  devise_scope :user do
    get '/users/sign_out(.:format)', to: 'devise/sessions#destroy'
  end

  mount RailsAdmin::Engine => '/manage', as: 'rails_admin'

  # Health check
  get '/health', to: 'health_check#index'

  # API
  namespace :api, defaults: {format: 'json'} do
    # V1
    scope module: :v1, constraints: DdrAux::Api::Constraints.new(version: 1, default: true) do

      resources :admin_sets, only: [:index, :show] do
        collection do
          get 'find'
        end
      end

      resources :alerts, only: [:index, :show] do
        collection do
          get 'active'
        end
      end

      resources :contacts, only: [:index, :show] do
        collection do
          get 'find'
        end
      end

      resources :job_queues, only: [:index, :show] do
        collection do
          get 'active'
          get 'resque_pool_config'
          patch '/', to: 'job_queues#index'
        end
      end

      resources :languages, only: [:index, :show] do
        collection do
          get 'find'
        end
      end

      resources :rights_statements, only: [:index, :show] do
        collection do
          get 'find'
        end
      end

      scope 'identifiers' do
        get    '/',    to: 'identifiers#index'
        post   '/',    to: 'identifiers#create'
        get    '/*id', to: 'identifiers#show'
        put    '/*id', to: 'identifiers#update'
        delete '/*id', to: 'identifiers#destroy'
      end
    end
  end

end

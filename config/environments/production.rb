Rails.application.configure do
  config.eager_load = true
  config.public_file_server.enabled = true
end

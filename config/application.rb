require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DdrAux
  class Application < Rails::Application

    config.active_record.time_zone_aware_types = [:datetime, :time]
    config.colorize_logging = false
    config.time_zone = 'Eastern Time (US & Canada)'
    config.log_level = ENV.fetch("RAILS_LOG_LEVEL", "info")

    # Required for RailsAdmin
    config.middleware.use ActionDispatch::Session::CookieStore, {:key=>"_ddr-aux_session", :cookie_only=>true}

    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)

  end
end

# load DdrAux.version
require 'ddr_aux'

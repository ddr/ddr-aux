RailsAdmin.config do |config|

  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  config.authorize_with :cancancan, RailsAdmin::Ability

  config.main_app_name = "DDR-Aux v#{DdrAux.version}"

  config.actions do
    dashboard
    index # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
  end

  config.model 'AdminSet' do
    list do
      field :code
      field :title
      field :created_at
      field :updated_at
    end
    show do
      field :code
      field :title
      field :created_at
      field :updated_at
    end
  end

  config.model 'Contact' do
    list do
      field :slug
      field :name
      field :url
    end
    show do
      field :slug
      field :name
      field :short_name
      field :url
      field :phone
      field :email
      field :ask
    end
  end

  config.model 'JobQueue' do
    list do
      sort_by :name
      field :name
      field :enabled
      field :paused
      field :workers
      field :description
      field :updated_at
    end
    show do
      field :name
      field :enabled
      field :paused
      field :workers
      field :description
      field :created_at
      field :updated_at
    end
  end

  config.model 'Language' do
    list do
      sort_by :code
      field :code do
        sort_reverse false
      end
      field :label
    end
    update do
      configure :code do
        read_only true
      end
    end
  end

  config.model 'RightsStatement' do
    list do
      field :title
      field :url
      field :short_title
      field :reuse_text
    end
    show do
      field :title
      field :url
      field :short_title
      field :feature
      field :reuse_text
      field :created_at
      field :updated_at
    end
    update do
      configure :url do
        read_only true
      end
    end
  end

  config.model 'User' do
    object_label_method do
      :to_s
    end
    list do
      field :username
      field :admin
      field :display_name
      field :email
    end
    show do
      field :admin
      field :username
      field :display_name
      field :email
      field :last_name
      field :first_name
      field :nickname
      field :created_at
      field :updated_at
    end
    edit do
      field :admin
      field :username
      field :display_name
      field :email
      field :last_name
      field :first_name
      field :nickname
    end
    visible do
      bindings[:controller].current_user.admin?
    end
  end

end

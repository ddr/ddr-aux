# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_20_172756) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_sets", force: :cascade do |t|
    t.string "code"
    t.string "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "alerts", force: :cascade do |t|
    t.string "site"
    t.datetime "start_at"
    t.datetime "stop_at"
    t.text "message"
    t.boolean "enabled", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string "slug"
    t.string "name"
    t.string "short_name"
    t.string "url"
    t.string "phone"
    t.string "email"
    t.string "ask"
  end

  create_table "job_queues", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", default: "", null: false
    t.integer "workers", default: 1, null: false
    t.boolean "enabled", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "paused", default: false
  end

  create_table "languages", force: :cascade do |t|
    t.string "code"
    t.string "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "manager_permissions", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "model", default: 0, null: false
    t.index ["user_id"], name: "index_manager_permissions_on_user_id"
  end

  create_table "rights_statements", force: :cascade do |t|
    t.string "title", limit: 255
    t.string "url", limit: 255
    t.string "short_title", limit: 255
    t.string "feature", limit: 255
    t.string "reuse_text", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "display_name"
    t.string "nickname"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "admin", default: false
    t.string "api_key"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
